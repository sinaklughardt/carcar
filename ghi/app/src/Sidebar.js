import { NavLink } from "react-router-dom"

export default function Sidebar() {
    return (
        <>
        <nav className="navbar navbar-expand-xxl navbar-dark bg-dark">
        <NavLink className="navbar-brand p-2" to="/">CarCar</NavLink>
        </nav>
    </>

    )
}
